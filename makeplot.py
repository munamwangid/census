# Import libraries

import json

from bokeh.io import show, output_file
from bokeh.plotting import figure
from bokeh.models import GeoJSONDataSource, LinearColorMapper
from bokeh.models import HoverTool, ColorBar
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh.palettes import brewer
import geopandas as gpd
import pandas as pd

# Data is obtained from
# https://adr.unaids.org/dataset/076ed6f1-5f7f-49d8-9f96-4ea2de9d0fa0/resource/0c5e7aa2-2afb-4617-bd7d-c77c0f585ee8/download/ken_areas.geojson
# https://www.knbs.or.ke/?wpdmpro=2019-kenya-population-and-housing-census-volume-i-population-by-county-and-sub-county#
# Read the geojson map file 
counties = gpd.read_file('ken_areas.geojson')
# Read in census population data
population = pd.read_csv('CountriesComputersTablets.csv')
# Rename columns
population.columns=['area_name','population']
# create boolean to indicate rows that are counties
is_county = counties['area_level']==2.0
# create a data frame with only counties
counties_only = counties[is_county]
# merge dataframe to include population data
merged = counties_only.merge(population, left_on='area_name',right_on='area_name',how='left')
# convert to geojson format
merged_json = json.loads(merged.to_json())
json_data = json.dumps(merged_json)
geo_source = GeoJSONDataSource(geojson = json_data)

# Indicate colors to use to show population, and to use 8 buckets to
palette = brewer['YlGnBu'][8]
palette = palette[::-1]
color_mapper = LinearColorMapper(palette = palette, low = 0, high = 30)
#tick_labels = {'2': 'Index 2', '3': 'Index 3', '4':'Index 4', '5':'Index 5', '6':'Index 6', '7':'Index 7', '8':'Index 8'}
color_bar = ColorBar(color_mapper=color_mapper, label_standoff=5, width = 650, height = 30,
border_line_color=None,location = (20,0), orientation = 'horizontal')

# Indicate information to be displayed when put mouse pointer over a county
hover = HoverTool(tooltips = [ ('County','@county'),('Computers', '@computers')])

# Generate plot
p = figure(title = 'Kenya map', plot_height = 600 , plot_width = 950, tools = [hover] )
p.xgrid.grid_line_color = None
p.ygrid.grid_line_color = None
# Indicate counties should be colored by population
p.patches('xs','ys', source = geo_source,fill_color = {'field' :'computers', 'transform' : color_mapper},
          line_color = 'black', line_width = 0.75, fill_alpha = 1)
p.add_layout(color_bar, 'below')

# Make an HTML page with the plot
html = file_html(p, CDN, "Kenya")
with open('index.html', 'w') as f:
    f.writelines(html)
